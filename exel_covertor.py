#!/usr/bin/env python3
import openpyxl

kontur_file_table = "./kontur.xlsx"
eva_file_table = "./eva.xlsx"

print("Начинаю загрузку таблици")
kontur_table = openpyxl.load_workbook(kontur_file_table)
print("Загрузка прошла успешна")

eva_table = openpyxl.Workbook()
sheet_e = eva_table.active

sheet_k = kontur_table.active

sheet_e.append(['Наименование','Артикул','Код','В продаже','Группа','Структура группы','Цена закупки','Цена','Остаток','Ед.изм.','Тип','Описание','Штрих-код','НДС','Частичная продажа','Тип собственности'])
row_max = sheet_k.max_row

print("Начинаю сздание таблици для эватора")
for row in range(2, row_max):
    item_group_0 = sheet_k[row][0].value
    item_group_1 = sheet_k[row][1].value
    item_group_2 = sheet_k[row][2].value
    code = sheet_k[row][3].value
    s_code = sheet_k[row][4].value
    art = sheet_k[row][5].value
    name = sheet_k[row][6].value
    count = sheet_k[row][10].value
    unit_m = sheet_k[row][11].value
    if unit_m == 'упак':
       unit_m = 'шт' 
    p_price = sheet_k[row][12].value
    s_price = sheet_k[row][13].value
    path_group=''
    final_group='default'
    if item_group_0:
        final_group = item_group_0
        if item_group_1:
            final_group = item_group_1
            path_group = f'{item_group_0} \\\ {item_group_1}'
            if item_group_2:
                final_group = item_group_2
                path_group = f'{item_group_0} \\\ {item_group_1} \\\ {item_group_2}'
    sheet_e.append([name, art, code, 'ИСТИНА', final_group, path_group, p_price, s_price, count, unit_m, 'NORMAL', '', s_code, 'NO_VAT','NEVER','OWN'])
    print(f"{int(row*100/row_max)}%", end='\r')
eva_table.save(eva_file_table)
print("Готово")